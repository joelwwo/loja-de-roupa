import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

import { produtos } from 'src/data/produtos';
import { IProduto } from './interfaces/Produto';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    produtos
    return { produtos };
  }

  genId(produtos: IProduto[]): number {
    return produtos.length > 0 ? Math.max(...produtos.map(produto => produto.id)) + 1 : 11;
  }
}