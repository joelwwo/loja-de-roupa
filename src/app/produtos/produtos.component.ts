import { ProdutosService } from './../produtos.service';
import { Component, OnInit } from '@angular/core';

import { IParametros } from '../interfaces/Paramentro';
import { IProduto } from '../interfaces/Produto'
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.scss']
})
export class ProdutosComponent implements OnInit {

  produtos: IProduto[] = []
  precoSelecionado = new FormControl(0);
  categoriaSelecionada = new FormControl('');
  produtosPorDisposicao = new FormControl()
  categorias = ['Casaco', 'Blusa', 'Calça', 'Vestido', 'Body']
  precos = [1, 2, 3, 4]
  busca: IParametros = {}

  constructor(private produtosService: ProdutosService) { }

  ngOnInit(): void {
    this.buscarProdutosPor()
  }

  limparFiltro() {
    this.categoriaSelecionada.setValue('')
    this.produtosPorDisposicao.setValue(false);
    this.precoSelecionado.setValue(0)
    this.busca = {}
    this.buscarProdutosPor()
  }

  async buscarMediaDasNotas(index) {

    let produto = this.produtos[index]

    const dividendo = produto.notas.reduce(function (total, nota) {
      return total + parseInt(nota.nota);
    }, 0);

    if (dividendo == 0) return 0

    const divisor = produto.notas.length
    const result = await this.produtosService.buscarMediaDasNotas(dividendo, divisor)
    this.produtos[index].mediaNota = result
  }

  getSifrao(preco: number): string {
    let cifras = ''
    for (let index = 0; index < preco; index++) {
      cifras += '$'
    }
    return cifras
  }

  getEstrelas(mediaNota): string {
    let estrelas = ''
    if (mediaNota == 0) return estrelas = '0'
    for (let index = 0; index < mediaNota; index++) {
      estrelas += '⭐'
    }
    return estrelas
  }

  salvarProdutoLocalStorage(produto: IProduto): void {
    localStorage.setItem('produto', JSON.stringify(produto))
  }

  configurarParametrosBusca(): string {

    if (this.precoSelecionado.value > 0) {
      this.busca.precoMedio = this.precoSelecionado.value
    }
    if (this.produtosPorDisposicao.value === true || this.produtosPorDisposicao.value === false) {
      this.busca.disponivel = this.produtosPorDisposicao.value
    }
    if (this.categoriaSelecionada.value) {
      this.busca.categoria = this.categoriaSelecionada.value
    }

    let parametros = ''

    if (this.busca) {

      for (let [key, value] of Object.entries(this.busca)) {
        parametros += key.toString() + '=' + value + '&'
      }
      parametros = parametros.slice(0, parametros.length - 1)

    }

    return parametros

  }

  buscarProdutosPor(): void {

    let parametros = this.configurarParametrosBusca()

    this.produtosService.buscarProdutosPor(parametros)
      .subscribe(produtos => {
        this.produtos = produtos
      })
  }

}
