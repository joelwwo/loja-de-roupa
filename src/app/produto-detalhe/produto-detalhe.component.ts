import { ProdutosService } from './../produtos.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProduto } from '../interfaces/Produto';
@Component({
  selector: 'app-produto-detalhe',
  templateUrl: './produto-detalhe.component.html',
  styleUrls: ['./produto-detalhe.component.scss']
})
export class ProdutoDetalheComponent implements OnInit {

  produto: IProduto
  indexAtualImagem = 0

  constructor(
    private route: ActivatedRoute,
    private produtosService: ProdutosService,
  ) { }

  ngOnInit(): void {
    this.buscarProduto()
  }

  ngOnDestroy(): void {
    localStorage.clear()

  }

  buscarProduto(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.produtosService.buscarProduto(id)
      .subscribe(produto => {
        this.produto = produto
        this.produto = { ...this.produto, ...this.produtoLocalStorage() }
      })
  }

  produtoLocalStorage(): IProduto {
    return JSON.parse(localStorage.getItem('produto'))
  }

  proximaImagem() {
    this.indexAtualImagem++
  }

  imagemAnterior() {
    this.indexAtualImagem--
  }

  getSifrao(preco: number): string {
    let cifras = ''
    for (let index = 0; index < preco; index++) {
      cifras += '$'
    }
    return cifras
  }

  getEstrelas(mediaNota): string {
    let estrelas = ''
    if (mediaNota == 0) return estrelas = '0'
    for (let index = 0; index < mediaNota; index++) {
      estrelas += '⭐'
    }
    return estrelas
  }

}
