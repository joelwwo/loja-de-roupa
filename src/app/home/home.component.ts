import { ProdutosService } from './../produtos.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  imagensDestacadas

  constructor(private produtosService: ProdutosService) { }

  ngOnInit(): void {
    this.buscarProdutosDestaque()
  }

  buscarProdutosDestaque(): void {
    this.imagensDestacadas = this.produtosService.buscarProdutosDestaque()

  }

}
