import { Injectable } from '@angular/core';
import { Observable, } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import axios from 'axios'

import { produtos } from './../data/produtos';
import { IProduto } from './interfaces/Produto';

@Injectable({
  providedIn: 'root'
})
export class ProdutosService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  produtos = produtos

  private produtosUrl = 'api/produtos';

  constructor(private http: HttpClient) { }

  buscarProdutos(): Observable<IProduto[]> {
    return this.http.get<IProduto[]>(this.produtosUrl)
  }

  buscarProduto(id: number): Observable<IProduto> {
    const url = `${this.produtosUrl}/${id}`;
    return this.http.get<IProduto>(url)
  }

  buscarProdutosDestaque() {

    let imagensDestacadas = []

    for (let index = 0; index < this.produtos.length - 1; index++) {
      let aux = this.produtos[index].imagens.length - 1
      for (let j = 0; j < aux; j++) {
        if (this.produtos[index].imagens.length && this.produtos[index].imagens[index].destacada) {
          imagensDestacadas.push(this.produtos[index].imagens[index])
        }

      }
    }

    return imagensDestacadas

  }

  buscarProdutosPor(busca?: string): Observable<IProduto[]> {
    const url = `${this.produtosUrl}/${busca ? '?' + busca : ''}`
    return this.http.get<IProduto[]>(url)
  }

  async buscarMediaDasNotas(dividendo: number = 10, divisor: number = 2) {
    let { data } = await axios.get(`http://api.mathjs.org/v4/?expr=${dividendo}/${divisor}`)
    return data.toFixed(1)
  }

}
