
export interface IParametros {
    categoria?: string
    precoMedio?: number
    disponivel?: boolean
    mediaNota?: number
}

