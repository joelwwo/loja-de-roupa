interface nota {
    nota: string
}

interface imagem {
    nome: string
    destacada: boolean
}

export interface IProduto {
    id: number
    nome: string
    categoria: string
    notas: nota[]
    imagens: imagem[]
    precoMedio: number
    disponivel: boolean
    mediaNota?: number

}

