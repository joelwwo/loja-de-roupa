import { ProdutoDetalheComponent } from './produto-detalhe/produto-detalhe.component';
import { PaginaNaoEncontradaComponent } from './pagina-nao-encontrada/pagina-nao-encontrada.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProdutosComponent } from './produtos/produtos.component';


const routes: Routes = [
  { path: 'destaques', component: HomeComponent },
  { path: '', redirectTo: 'destaques', pathMatch: 'full' },
  { path: 'produtos', component: ProdutosComponent },
  { path: 'detalhe/:id', component: ProdutoDetalheComponent },
  { path: '**', component: PaginaNaoEncontradaComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
