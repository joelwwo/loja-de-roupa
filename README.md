# Joel dos Santos Silva
## GitHub: [https://github.com/joelwwo](https://github.com/joelwwo)

___

## Para testar basta...

1. Possuir o Node v12.17.0
2. NPM
3. Entrar no diretório raiz do projeto
4. Digitar o comando: npm start
5. Acessar o endereço no navegador: [http://localhost:4200/](http://localhost:4200/)
